// ---------- express ----------- //

require('dotenv').config()
const express = require('express')

const app = express()
const PORT = process.env.PORT || 5000

app.get('/', (req, res) => {
  res.send('Hello there')
})

app.listen(PORT, () => {
  console.log(`App listening on port ${PORT}`)
})


// ---------- bots ------------ //
const Bot = require('./bot')

const racoon = new Bot(require('./bots/racoon.json'))

if (process.env.SLEEPING === 'false') {
  racoon.login()
}