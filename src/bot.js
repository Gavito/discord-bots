const Discord = require('discord.js')

// ------ possible actions ------- //

const actions = {
  help: require('./actions/help'),
  greet: require('./actions/greet'),
  hello: require('./actions/hello'),
  civilwar: require('./actions/civilwar'),
  play: require('./actions/music/play'),
  queue: require('./actions/music/queue'),
  pause: require('./actions/music/pause'),
  resume: require('./actions/music/resume'),
  skip: require('./actions/music/skip'),
  disconnect: require('./actions/music/disconnect'),
}


// ------ command handling ------- //

const handleCommand = (bot, message) => {
  let { prefix, commands, commandNotFound } = bot

  let terms = message.content.trim().slice(prefix.length).split(' ')
  let command = terms[0]
  let args = terms.slice(1)

  if (!Object.keys(commands).includes(command)) {
    message.channel.send(commandNotFound)
    return
  }

  console.log(`${message.author.username} requested the command ${command}`)
  return actions[commands[command]].run(bot, message, args).catch(err => { console.log(err) })
}


// -------- bot object --------- //

module.exports = class Bot {
  constructor(bot) {

    for (let prop in bot) {
      this[prop] = bot[prop]
    }

    this.client = new Discord.Client()
    this.queues = new Map()

    // get command    
    this.client.on('message', async message => {  
      if ( //ignore messages that falls on this rules
        message.author.bot ||
        message.channel.type == 'dm' ||
        !message.content.startsWith(this.prefix) ||
        message.content.startsWith(`<@!${this.client.user.id}`) ||
        message.content.startsWith(`<@${this.client.user.id}`)
      ) return
      
      handleCommand(this, message)
    })
  }

  login() {
    this.client.login(this.token)
  }

  setServerQueue(serverId, queue) {
    if (!queue) {
      queue = {
        textChannel: '',
        voiceChannel: '',
        connection: null,
        songs: [],
        volume: 1,
        playing: true
      }
    }
    if (!this.queues.has(serverId)) this.queues.set(serverId, queue)
  }

  queue(serverId, song) {
    let queue = this.queues.get(serverId)
    queue.songs.push(song)
    this.queues.set(serverId, queue)
  }

  next(serverId) {
    let queue = this.queues.get(serverId)
    let song = queue.songs.shift()
    this.queues.set(serverId, queue)
    return song
  }
}