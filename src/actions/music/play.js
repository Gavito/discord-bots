const ytdl = require('ytdl-core')
const ytsr = require('ytsr')

module.exports.run = async (bot, message, args) => {
  const serverId = message.guild.id
  const voiceChannel = message.member.voice.channel;
    
  let queue = {
    textChannel: message.channel,
    voiceChannel: voiceChannel,
    connection: null,
    songs: [],
    volume: 1,
    playing: false,
    dispatcher: null
  }
  
  if (!voiceChannel) {
    return message.channel.send("How do you think you're gonna listen to anything outside a voice channel?");
  }
  
  if (!args || !args.length) {
    return message.channel.send("Play what? You have to tell what I'm supposed to play, don't you think?")
  }

  if (!bot.queues.has(serverId)) {
    try {
      var connection = await voiceChannel.join()
      connection.on('disconnect', () => {
        bot.queues.delete(serverId)
        return message.channel.send("Ok, stopping the music.")
      })
      queue.connection = connection
    } catch (err) {
      console.log(err);
      return message.channel.send("Its seems like something wrong happened. My bad.");
    }

    bot.setServerQueue(serverId, queue)
  } else {
    queue = bot.queues.get(serverId)
  }

  let search = args[0].startsWith('http') ? url = args[0] : args.join(" ")
  let searchResults = await ytsr(search, { limit: 5 })
  let results = searchResults.items.filter(el => el.type === 'video')
  let video = results.length && results[0]

  console.log(video);
  
  if (!video) {
    return message.channel.send("I didn't find anything about what you said.");
  }
  
  let song = { ...video }

  if (!queue.playing) {
    queue.songs.push(song)
    play(queue)
  } else {
    queue.textChannel.send(`Ok, adding **${song.title}** to the list  `);
    bot.queue(serverId, song)
  }
}

const play = (queue) => {
  if (!queue.songs || !queue.songs.length) {
    queue.voiceChannel.leave()
    console.log('sem musicas');
    return
  }

  let song = queue.songs[0]

  queue.textChannel.send(`Ok, now let's play: **${song.title}**`);
  queue.playing = true
  
  const dispatcher = queue.connection
    .play(ytdl(song.url, { filter: 'audioonly' }))
    .on("finish", () => {
      queue.songs.shift()
      play(queue, queue.songs[0])
    })
    .on("disconnect", () => {
      queue.songs = []
      // return message.channel.send("Ok, stopping the music!");
      console.log('disconnected')
    })
    .on("error", error => console.error(error))

  queue.dispatcher = dispatcher
}

exports.play = play