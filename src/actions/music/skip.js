const playCommand = require('./play')

module.exports.run = async (bot, message, args) => {
  const serverId = message.guild.id
  const voiceChannel = message.member.voice.channel

  if (!voiceChannel) {
    return message.channel.send("You're not in a voice channel dude")
  }

  let queue = {}
  let noSong = "There's no song to skip to"

  if (bot.queues.has(serverId)) {
    queue = bot.queues.get(serverId)
  } else return message.channel.send(noSong)

  if (queue.songs.length > 1) {
    message.channel.send("Ok, skipping this song")
    queue.dispatcher = null
    queue.playing = false
    let num = args.length && Number(args[0])
    if (num && !Number.isNaN(num) && num > 0) {
      queue.songs = queue.songs.slice(num)
    } else {
      queue.songs.shift()
    }
    playCommand.play(queue)
  } else return message.channel.send(noSong)
}