module.exports.run = async (bot, message, args) => {
  const serverId = message.guild.id
  const voiceChannel = message.member.voice.channel

  if (!voiceChannel) {
    return message.channel.send("You're not in a voice channel to disconnect me, you little shit");
  }

  if (bot.queues.has(serverId)) {
    message.channel.send("Ok, I'm leaving, you don't have to say twice");
    bot.queues.get(serverId).connection.disconnect()
    bot.queues.delete(serverId)
  }
}