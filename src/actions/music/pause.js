module.exports.run = async (bot, message, args) => {
  const serverId = message.guild.id
  const voiceChannel = message.member.voice.channel

  if (!voiceChannel) {
    return message.channel.send("You're not in a voice channel dude");
  }

  let queue = {}
  const nothingToPause = "Pause what? There is nothing to pause?"

  if (bot.queues.has(serverId)) {
    queue = bot.queues.get(serverId)
  } else return message.channel.send(nothingToPause)

  if (queue.playing) {
    message.channel.send("Ok pausing the song, you party pooper")
    queue.dispatcher && queue.dispatcher.pause()
    queue.playing = false
  } else return message.channel.send(nothingToPause)
}