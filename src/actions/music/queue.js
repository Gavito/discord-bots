const Discord = require('discord.js');

module.exports.run = async (bot, message, args) => {
  const serverId = message.guild.id
  const voiceChannel = message.member.voice.channel

  if (!voiceChannel) {
    return message.channel.send("You're not in a voice channel dude");
  }

  let queue = {}

  if (!bot.queues.has(serverId)) {
    return message.channel.send("There is no queue here. But if you wanna play something you have to tell me, you know?");
  } else {
    queue = bot.queues.get(serverId)
  }

  let content = ''
  queue.songs.forEach((song, i) => {
    content += `\n**${i === 0 && queue.playing ? '(playing now)' : ''} ${i+1}** - [${song.title}](${song.url}) **${song.duration}**\n`
  })

  const embed = new Discord.MessageEmbed()
  .setTitle("Here's the song list")
  .setDescription(content)
  message.channel.send(embed)
}