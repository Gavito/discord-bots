module.exports.run = async (bot, message, args) => {
  const serverId = message.guild.id
  const voiceChannel = message.member.voice.channel

  if (!voiceChannel) {
    return message.channel.send("You're not in a voice channel dude");
  }

  let queue = {}
  if (bot.queues.has(serverId)) {
    queue = bot.queues.get(serverId)
  } else return message.channel.send("Resume what? The song list is empty")

  if (!queue.playing) {
    message.channel.send(`Yeah! Let's get back! Playing **${queue.songs[0].title}**`) 
    queue.dispatcher && queue.dispatcher.resume()
    queue.playing = true
  } else return message.channel.send("Already playing bro...") 
}