const Discord = require('discord.js')

const text = `
Ok. Here is a list of things you can ask me, but don't abuse.

**COMMON**

**!help**  ⮚  I'll show you what to do, like I am doing now.
**!greet**  ⮚  I'm gonna tell how I felt about meeting you.
**!hi**  ⮚  I'll be nice for once. Maybe not.
`

module.exports.run = async (bot, message, args) => {
  let embed = new Discord.MessageEmbed()
  .setTitle('What you can ask')
  .setDescription(text)
  .setColor('RED')

  let msgEmbed = await message.channel.send(embed)
}