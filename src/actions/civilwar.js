const Discord = require('discord.js')

const roles = {
  '😇': '#teamCap',
  '😎': '#teamIronMan',
  '⭐': '#teamGuardians'
}

const description = `
The avengers are fighting each other because they are a bunch of losers.

So Stark thinks they should sign to the Sokovia Accord and become puppies of their government but Cap thinks they should be free to destroy anything anywhere they want.

Which moron do you wanna follow? 

If you wanna be a part of #teamCap click :innocent: 

If you wanna be a part of #teamIronMan click :sunglasses: 

BUT... If you wanna be a part of the best team of the galaxy (you know which that is) click :star:

It's up to you!
`

module.exports.run = async (bot, message, args) => {
  const channelID = message.channel.id

  let embed = new Discord.MessageEmbed()
  .setTitle('Civil War')
  .setDescription(description)
  .setColor('RED')

  let msgEmbed = await message.channel.send(embed)
  
  let emojis = Object.keys(roles)

  msgEmbed.react(emojis[0])
			.then(() => msgEmbed.react(emojis[1]))
			.then(() => msgEmbed.react(emojis[2]))
			.catch(() => console.error('One of the emojis failed to react.'));


  let handleReaction = (reaction, user, add) => {
    if (user.bot) return

    const emoji = reaction._emoji.name
    const { guild } = reaction.message

    let roleName = roles[emoji]

    if (!roleName) return

    const role = guild.roles.cache.find(role => role.name === roleName)
    const member = guild.members.cache.find(member => member.id === user.id)

    if (add) member.roles.add(role)
    else member.roles.remove(role)
  }

  bot.client.on('messageReactionAdd', (reaction, user) => {
    if (channelID === reaction.message.channel.id) {
      handleReaction(reaction, user, true)
    }
  })

  bot.client.on('messageReactionRemove', (reaction, user) => {
    if (channelID === reaction.message.channel.id) {
      handleReaction(reaction, user, false)
    }
  })
}